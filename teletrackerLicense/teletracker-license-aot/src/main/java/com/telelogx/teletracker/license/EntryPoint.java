package com.telelogx.teletracker.license;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.excelsior.service.WinService;

/**
 * @author Mohmmad Qasim
 *
 */
public class EntryPoint extends WinService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void run() {
	Startup.main(new String[] {});
    }

}
