/**
 * 
 */
package com.colureware.configuration.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

import com.colureware.configuration.Configuration;

/**
 * @author yazan
 * 
 */
public class ConfigurationImpl implements Configuration {

    private XMLConfiguration configuration;
    private ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    /**
     * @param xmlConfiguration
     */
    public ConfigurationImpl(XMLConfiguration xmlConfiguration) {
	this.configuration = xmlConfiguration;
    }

    /**
     * 
     */
    public ConfigurationImpl() {

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.colureware.configuration.Configuration#getProperty(java.lang.String)
     */
    @Override
    public Object getProperty(String name) {
	return configuration.getProperty(name);
    }

    @Override
    public char getListDelimiter() {
	return configuration.getListDelimiter();
    }

    @Override
    public void setListDelimiter(char listDelimiter) {
	configuration.setListDelimiter(listDelimiter);
    }

    @Override
    public boolean containsKey(String key) {
	return configuration.containsKey(key);
    }

    @Override
    public Iterator getKeys() {
	return configuration.getKeys();
    }

    @Override
    public Iterator getKeys(String prefix) {
	return configuration.getKeys(prefix);
    }

    @Override
    public void addProperty(String key, Object value) {
	configuration.addProperty(key, value);
    }

    @Override
    public Properties getProperties(String key) {
	return configuration.getProperties(key);
    }

    @Override
    public Properties getProperties(String key, Properties defaults) {
	return configuration.getProperties(key, defaults);
    }

    @Override
    public boolean getBoolean(String key) {
	return configuration.getBoolean(key);
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
	return configuration.getBoolean(key, defaultValue);
    }

    @Override
    public Boolean getBoolean(String key, Boolean defaultValue) {
	return configuration.getBoolean(key, defaultValue);
    }

    @Override
    public byte getByte(String key) {
	return configuration.getByte(key);
    }

    @Override
    public byte getByte(String key, byte defaultValue) {
	return configuration.getByte(key, defaultValue);
    }

    @Override
    public Byte getByte(String key, Byte defaultValue) {
	return configuration.getByte(key, defaultValue);
    }

    @Override
    public double getDouble(String key) {
	return configuration.getDouble(key);
    }

    @Override
    public double getDouble(String key, double defaultValue) {
	return configuration.getDouble(key, defaultValue);
    }

    @Override
    public Double getDouble(String key, Double defaultValue) {
	return configuration.getDouble(key, defaultValue);
    }

    @Override
    public float getFloat(String key) {
	return configuration.getFloat(key);
    }

    @Override
    public float getFloat(String key, float defaultValue) {
	return configuration.getFloat(key, defaultValue);
    }

    @Override
    public Float getFloat(String key, Float defaultValue) {
	return configuration.getFloat(key, defaultValue);
    }

    @Override
    public int getInt(String key) {
	return configuration.getInt(key);
    }

    @Override
    public int getInt(String key, int defaultValue) {
	return configuration.getInt(key, defaultValue);
    }

    @Override
    public Integer getInteger(String key, Integer defaultValue) {
	return configuration.getInteger(key, defaultValue);
    }

    @Override
    public long getLong(String key) {
	return configuration.getLong(key);
    }

    @Override
    public long getLong(String key, long defaultValue) {
	return configuration.getLong(key, defaultValue);
    }

    @Override
    public Long getLong(String key, Long defaultValue) {
	return configuration.getLong(key, defaultValue);
    }

    @Override
    public short getShort(String key) {
	return configuration.getShort(key);
    }

    @Override
    public short getShort(String key, short defaultValue) {
	return configuration.getShort(key, defaultValue);
    }

    @Override
    public Short getShort(String key, Short defaultValue) {
	return configuration.getShort(key, defaultValue);
    }

    @Override
    public BigDecimal getBigDecimal(String key) {
	return configuration.getBigDecimal(key);
    }

    @Override
    public BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
	return configuration.getBigDecimal(key, defaultValue);
    }

    @Override
    public BigInteger getBigInteger(String key) {
	return configuration.getBigInteger(key);
    }

    @Override
    public BigInteger getBigInteger(String key, BigInteger defaultValue) {
	return configuration.getBigInteger(key, defaultValue);
    }

    @Override
    public String getString(String key) {
	return configuration.getString(key);
    }

    @Override
    public String getString(String key, String defaultValue) {
	return configuration.getString(key, defaultValue);
    }

    @Override
    public String[] getStringArray(String key) {
	return configuration.getStringArray(key);
    }

    @Override
    public int getMaxIndex(String key) {
	return configuration.getMaxIndex(key);
    }

    @Override
    public List getList(String key) {
	return configuration.getList(key);
    }

    @Override
    public List getList(String key, List defaultValue) {
	return configuration.getList(key, defaultValue);
    }

    /**
     * @param name
     * @param value
     */
    @Override
    public void putProperty(String name, Object value) {
	try {
	    readWriteLock.writeLock().lock();
	    configuration.setProperty(name, value);
	} finally {
	    readWriteLock.writeLock().unlock();
	}

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.colureware.configuration.Configuration#save()
     */
    @Override
    public void save() throws IOException {
	try {
	    readWriteLock.writeLock().lock();
	    configuration.save();
	} catch (ConfigurationException e) {
	    throw new IOException("Can't save configuration", e);
	} finally {
	    readWriteLock.writeLock().unlock();
	}
    }

    /**
     * @return the configuration
     */
    public XMLConfiguration getConfiguration() {
	return configuration;
    }

    /**
     * @param configuration
     *            the configuration to set
     */
    public void setConfiguration(XMLConfiguration configuration) {
	this.configuration = configuration;
    }

    /**
     * @param properties
     */
    @Override
    public void putAll(Map<String, Object> properties) {
	Set<String> keySet = properties.keySet();
	for (String key : keySet) {
	    putProperty(key, properties.get(key));
	}

    }
}
