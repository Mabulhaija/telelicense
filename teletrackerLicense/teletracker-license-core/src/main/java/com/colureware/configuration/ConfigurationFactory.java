/**
 * 
 */
package com.colureware.configuration;

/**
 * @author yazan
 * 
 */
public interface ConfigurationFactory {

    /**
     * @return
     */
    public Configuration createConfiguration();
    
    /**
     * @param configName
     * @return
     */
    public Configuration createConfiguration(String configName);
}
