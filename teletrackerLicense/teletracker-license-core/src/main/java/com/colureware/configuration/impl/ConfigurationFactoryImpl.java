/**
 * 
 */
package com.colureware.configuration.impl;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colureware.configuration.Configuration;
import com.colureware.configuration.ConfigurationFactory;

/**
 * @author yazan
 * 
 */
public class ConfigurationFactoryImpl implements ConfigurationFactory {

    private  Logger logger = LoggerFactory.getLogger(ConfigurationFactoryImpl.class);

    private static final String DEFAUL_GLOBAL_CONFIG_FILE_NAME = "app-props.xml";
    public static final String GLOBAL_CONFIG_DIR_SYS_PROP = "global-config-dir";
    private static Configuration configurationSingelton;
    
    @Override
    public synchronized Configuration createConfiguration() {
	return createConfiguration(DEFAUL_GLOBAL_CONFIG_FILE_NAME);
    }

    @Override
    public synchronized Configuration createConfiguration(String configName) {
	if (configurationSingelton == null) {
	    try {
		String configFileName = null;
		String configDir = System.getProperty(GLOBAL_CONFIG_DIR_SYS_PROP);
		if (configDir != null) {
		    logger.debug(String.format("config dir is not null (%1$s)", configDir));
		    configFileName = configDir + "/" + configName;
		} else {
		    logger.debug("config dir is null");
		    configFileName = configName;
		}
		File configFile = new File(configFileName).getAbsoluteFile();
		logger.debug(String.format("config file location is (%1$s)", configFile));
		configurationSingelton = new ConfigurationImpl(new XMLConfiguration(configFile.toURI().toURL()));
	    } catch (ConfigurationException | MalformedURLException e) {
		logger.error("Couldn't load props", e);
		throw new RuntimeException("Can't init properties", e);
	    }
	}
	return configurationSingelton;
    }

}
