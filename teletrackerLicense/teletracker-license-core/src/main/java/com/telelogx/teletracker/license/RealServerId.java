package com.telelogx.teletracker.license;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import com.colureware.common.ObfUtil;
import com.colureware.common.security.HashingUtility;
import com.colureware.license.PlatformInfo;

/**
 * @author yazan
 * 
 */
@Component
public class RealServerId implements PlatformInfo {

    /*
     * (non-Javadoc)
     * 
     * @see com.colureware.license.PlatformInfo#getPlatformUniqueID()
     */
    @Override
    public String getPlatformUniqueID() {
	try {
	    String result = getMotherboardSN() + getCPUSN();
	    String uniqueID = HashingUtility.encodeMD5(result);
	    return Base64.encodeBase64String(uniqueID.getBytes("utf-8"));
	} catch (UnsupportedEncodingException | GeneralSecurityException e) {
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    throw new RuntimeException(e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * 
     * @see com.colureware.license.PlatformInfo#isVirtualMachine()
     */
    @Override
    public boolean isVirtualMachine() {
	try {
	    String motherBoard = getMotherboardManufacturer();
	    motherBoard.toLowerCase();
	    return (motherBoard.equals(new de.schlichtherle.util.ObfuscatedString(new long[] { 0x27DBD52636F8B5A3L,
		    0x5F548AB56B34BC67L, 0xD5DA26914724B1EBL, 0x573869D328EBE3F6L }).toString() /*
												 * =
												 * >
												 * "microsoft corporation"
												 */)
		    || motherBoard.contains(new de.schlichtherle.util.ObfuscatedString(new long[] {
			    0xF5AA79B798927D8BL, 0xCFA49572B58FF304L }).toString() /*
										    * =
										    * >
										    * "vmware"
										    */) || motherBoard
			.contains(new de.schlichtherle.util.ObfuscatedString(new long[] { 0x51D942B308586CCEL,
				0x82FBA6C4284223B3L, 0x2C801D7762243657L }).toString() /*
										        * =
										        * >
										        * "virtualbox"
										        */));
	} catch (IOException e) {
	    throw new RuntimeException(e);
	}
    }

    /**
     * @return
     * @throws IOException
     */
    protected String getCPUSN() throws IOException {
	String vbs = new de.schlichtherle.util.ObfuscatedString(new long[] { 0x8602FC6F3D71A94FL, 0x1A3ACC0445A9CCD9L,
		0xE729920344821021L, 0xC8185FD4890372DEL, 0x400002B0AC37B790L, 0x4DE1AEB6944C141AL,
		0x11C40EA2E08BF6E6L, 0x65CCA90ADEF8DA96L, 0xEC554F948CA6F8D1L }).toString() /*
											     * =
											     * >
											     * "Set
											     * objWMIService
											     * =
											     * GetObject
											     * (
											     * \
											     * "winmgmts:\\\\.\\root\\cimv2\"
											     * )
											     * "
											     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x9C311D77E972B6EEL, 0x80D2E257EB8531L,
			0xC94685DF5A38C5D8L, 0x37E4807A47608126L, 0xAB27AC807FDAAA85L, 0x37D48CAC943012C6L,
			0xC74691EFEBEF1F62L }).toString() /*
							   * => "Set colItems =
							   * objWMIService
							   * .ExecQuery _ "
							   */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0xC288488C99215381L, 0x379D92B5E7CB1908L,
			0xE24FCF15DEFFE4E5L, 0x682257C714926318L, 0xEA06607F2A6C7967L, 0x8A0BCC9516151B3FL })
			.toString() /*
				     * => " (\"Select * from Win32_Processor\" )
				     * "
				     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0xB121D2A3D3CBE20FL, 0x8450CCA142A4635CL,
			0x5D71725A32B773F4L, 0x2164DCF0A7B99709L, 0xBE08EF9B9D54D7F1L }).toString() /*
												     * =
												     * >
												     * "For
												     * Each
												     * objItem
												     * in
												     * colItems
												     * "
												     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x85E29029FEB193B7L, 0x451803F38EE6DAFDL,
			0xE8D33265CB6B9044L, 0x9A6FEE164C1A3A7AL, 0x71C5930294FABB75L, 0xDEBCF0D2391BEE35L })
			.toString() /*
				     * => " Wscript.Echo objItem.ProcessorId "
				     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x3554D17D192D19D4L, 0x6175FD832D66303DL,
			0x8F128A5B3E07CE78L, 0xF87BE7C991EC9AC8L, 0x9D93212D23302D44L, 0xF83D306486A548DCL })
			.toString() /*
				     * => " exit for ' do the first cpu only! "
				     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x70AFF629CFC11F81L, 0xFEEBA68D630F7102L })
			.toString() /*
				     * => "Next "
				     */;
	return executeScript(vbs);
    }

    /**
     * @return
     * @throws IOException
     */
    protected String getMotherboardSN() throws IOException {
	String vbs = new de.schlichtherle.util.ObfuscatedString(new long[] { 0x428FDF7F199D98A2L, 0x6E702E7B3B8B8F1DL,
		0x3A4975CB16889DBCL, 0x1411366D3A603AE9L, 0xA6A7EEA540AA368BL, 0x3C08FE7414DB14BAL,
		0x11D4EC0F133EAEB0L, 0x35B34FED4A13D6B0L, 0xABA3AFAEEB97E4B5L }).toString() /*
											     * =
											     * >
											     * "Set
											     * objWMIService
											     * =
											     * GetObject
											     * (
											     * \
											     * "winmgmts:\\\\.\\root\\cimv2\"
											     * )
											     * "
											     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0xC9216AADFC175C85L, 0xE2DEE39863119CE9L,
			0x11C45D52E2114DCCL, 0x161DB3375ED2B934L, 0xFAAF43F1CFBA52DAL, 0xC9CD6101A0A589C3L,
			0x758993D620BE6C8EL }).toString() /*
							   * => "Set colItems =
							   * objWMIService
							   * .ExecQuery _ "
							   */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0xC94D440669233E7EL, 0x5A71F806281359DEL,
			0xD396C344773CA160L, 0xD5C271D503ADEF9FL, 0x7FB3AB67DF945707L, 0x12B3584738FA095BL })
			.toString() /*
				     * => " (\"Select * from Win32_BaseBoard\" )
				     * "
				     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x5B48E666AB89E878L, 0xFDFA994DAF7FD64BL,
			0x6BCEB513FAF68170L, 0xC350DBDF9C407AFFL, 0x67E040244CD55B1DL }).toString() /*
												     * =
												     * >
												     * "For
												     * Each
												     * objItem
												     * in
												     * colItems
												     * "
												     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0xB353FC63C3602E92L, 0x57B0DCEDA68FE46L,
			0x27AF7CF584D9CDE1L, 0x90FAC3B6031A4F0CL, 0x35715BBA4940A15CL, 0xBECE27A711B498F0L })
			.toString() /*
				     * => " Wscript.Echo objItem.SerialNumber "
				     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x4A7D32EC0DE2C849L, 0xF09E23C81289706CL,
			0xDBA8EA2FE76F0325L, 0xF5AEB5C3A08EB1F4L, 0xC1EDA42370BA3D22L, 0x4AE9E7581EBABDB7L })
			.toString() /*
				     * => " exit for ' do the first cpu only! "
				     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x47D1F0D0421BD7E8L, 0x7E65E279575BD62EL })
			.toString() /*
				     * => "Next "
				     */;
	return executeScript(vbs);
    }

    /**
     * @return
     * @throws IOException
     */
    protected String getMotherboardManufacturer() throws IOException {
	String vbs = new de.schlichtherle.util.ObfuscatedString(new long[] { 0x149FDD76EA74AA2L, 0xB5A8BC21572454C4L,
		0xFB630B41A4AE63CDL, 0x890E4AE1FEE328A0L, 0xAEA77FD9A3BBE3F9L, 0x94F7A1C273AD6D73L, 0xF38CCF060F18653L,
		0xA32F26F6C12D7C63L, 0x4155CD027FD89074L }).toString() /*
								        * =>
								        * "Set
								        * objWMIService
								        * =
								        * GetObject
								        * (\
								        * "winmgmts:\\\\.\\root\\cimv2\"
								        * ) "
								        */
		+ ObfUtil.obfs("Set colItems = objWMIService.ExecQuery _ \n"
			+ "   (\"Select * from Win32_BaseBoard\" ) \n")
		+ ObfUtil.obfs("For Each objItem in colItems \n" + "    Wscript.Echo objItem.Manufacturer \n")
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x2C49901FC855407EL, 0x8F6673A6F1BAE9CFL,
			0x10822C54B8805CDDL, 0x2E86109A480878D8L, 0x8F2BC12E4BF43C07L, 0xFB682D56509781ACL })
			.toString() /*
				     * => " exit for ' do the first cpu only! "
				     */
		+ new de.schlichtherle.util.ObfuscatedString(new long[] { 0x2F1821CCBF431FD6L, 0xD9F41E8E93E7C272L })
			.toString() /*
				     * => "Next "
				     */;
	return executeScript(vbs);
    }

    /**
     * @param vbs
     * @return
     * @throws IOException
     */
    private String executeScript(String vbs) throws IOException {
	String result;
	File file = writeScriptFile(vbs);
	Process process = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
	result = readAllOutput(process);
	return result.trim();
    }

    /**
     * @param vbs
     * @return
     * @throws IOException
     */
    private File writeScriptFile(String vbs) throws IOException {
	File file = File.createTempFile("ccc", ".vbs");
	file.deleteOnExit();
	try (FileWriter fw = new java.io.FileWriter(file)) {
	    fw.write(vbs);
	    return file;
	}
    }

    /**
     * @param result
     * @return
     * @throws IOException
     */
    private String readAllOutput(Process p) throws IOException {
	try (BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
	    String result = "";
	    String line;
	    while ((line = input.readLine()) != null) {
		result += line;
	    }
	    return result;
	}
    }

    /**
     * @param args
     * @throws Exception
     */
    public void DummyMain() throws Exception {
	String motherboardId = getMotherboardSN();
	javax.swing.JOptionPane.showConfirmDialog((java.awt.Component) null, motherboardId,
		"Motherboard serial number", javax.swing.JOptionPane.DEFAULT_OPTION);

	String cpuId = getCPUSN();
	javax.swing.JOptionPane.showConfirmDialog((java.awt.Component) null, cpuId, "CPU serial number",
		javax.swing.JOptionPane.DEFAULT_OPTION);

	String manufacturer = getMotherboardManufacturer();
	javax.swing.JOptionPane.showConfirmDialog((java.awt.Component) null, manufacturer, "Manufacturer",
		javax.swing.JOptionPane.DEFAULT_OPTION);

	String encoded = HashingUtility.encodeMD5(getMotherboardSN() + getCPUSN());
	javax.swing.JOptionPane.showConfirmDialog((java.awt.Component) null, encoded, "Encoded ID",
		javax.swing.JOptionPane.DEFAULT_OPTION);
    }

    public static void main(String[] args) {
	RealServerId gr = new RealServerId();
	System.out.println(gr.getPlatformUniqueID());
    }
}

