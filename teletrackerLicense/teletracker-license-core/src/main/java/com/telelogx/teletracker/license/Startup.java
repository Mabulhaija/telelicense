/**
 * 
 */
package com.telelogx.teletracker.license;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.colureware.util.StartupUtils;

/**
 * @author Mohammad
 *
 */
public class Startup {

    /**
     * @param args
     */
    public static void main(String[] args) {
	ClassPathXmlApplicationContext context = StartupUtils
		.startClasspathXMLApplicationContext("spring/app-context.xml");
	TeletrackerLicenseManager licenseManager = context.getBean(TeletrackerLicenseManager.class);
	try {
	    licenseManager.install();
	    licenseManager.verfyLecinces();
	    StartupUtils.waitInfinatly();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
