package org.telelogx.teletracker.license;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class JettyMethods extends AbstractHandler {

    private String operator;
    private String agentId;
    private TeletrackerLicenseDAO dataBaseInstance = new TeletrackerLicenseDAO();

    private void init(HttpServletRequest request, HttpServletResponse response, Request baseRequest) {
	response.setContentType("text/html;charset=utf-8");
	operator = request.getParameter("op");

	baseRequest.setHandled(true);
    }

    private void addAgentID(HttpServletResponse response, HttpServletRequest request) throws IOException, SQLException {

	if (getAgentID(request) == "") {

	    try {
		response.getWriter().println("<h1>You have to enter AgentID!</h1>");
		dataBaseInstance.traceOperation("ADD", agentId, "Failed", "Try to insert new agent without AgentID.");
		response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
	    } catch (IOException e) {
		System.out.println("Error While send response");
		e.printStackTrace();
	    }
	} else {
	    dataBaseInstance.checkAgentIDBeforAdd(response, agentId);

	}
    }

    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException {
	init(request, response, baseRequest);

	switch (operator) {
	case "add":
	    try {
		addAgentID(response, request);
	    } catch (SQLException e) {
		dataBaseInstance.traceOperation("ADD", getAgentID(request), "Failed", "Error while add to DB.");
		e.printStackTrace();
	    }
	    break;
	case "rmv":
	    try {
		removeAgentID(response, request);
	    } catch (SQLException e) {
		dataBaseInstance.traceOperation("REMOVE", getAgentID(request), "Failed",
			"Error while removing from DB.");
		e.printStackTrace();
	    }
	    break;
	case "auth":


	    try {
		checkRecivedAgentID(request, response);
	    } catch (SQLException e) {
		dataBaseInstance.traceAuthrization(getAgentID(request), "Failed", "Error while cheking DB.");
		e.printStackTrace();
	    }

	    break;
	case "lst":
	    dataBaseInstance.listAgentsInDB(response);
	    break;
	default:
	    response.getWriter().println("<h1>Wrong parameter Check enterd parameters please:</h1>");
	    response.getWriter().println("<h2>To add new agent set: op=add</h2>");
	    response.getWriter().println("<h2>To remove exist agent set: op=rmv</h2>");
	    response.getWriter().println("<h2>To list authorized agents set: op=lst</h2>");
	}

    }

    private void checkRecivedAgentID(HttpServletRequest request, HttpServletResponse response)
 throws SQLException,
	    IOException {
	String saltedAgent = request.getParameter("id");
	String dummyString = saltedAgent.substring(saltedAgent.length() - 5);
	String originalAgent = saltedAgent.substring(0, saltedAgent.length() - 5);
	if (saltedAgent == "")
	    dataBaseInstance.traceAuthrization(originalAgent, "Failed", "There is no AgentID recived");
	else {
	    String mixedDummyString = dummyString + "a28mPxx";
	    dataBaseInstance.authorizationChecker(originalAgent, saltedAgent, mixedDummyString, response);

	}
    }

    private void removeAgentID(HttpServletResponse response, HttpServletRequest request) throws IOException,
	    SQLException {

	if (getAgentID(request) == "") {

	    try {
		response.getWriter().println("<h1>You have to enter AgentID!</h1>");
		dataBaseInstance.traceOperation("REMOVE", agentId, "Failed", "Try to remove agent without AgentID.");
		response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
	    } catch (IOException e) {
		System.out.println("Error While send response");
		e.printStackTrace();
	    }
	} else {

	    dataBaseInstance.removeAgentFromDB(response, agentId);
	}

    }

    private String getAgentID(HttpServletRequest request) {
	if (request.getParameter("id") != null) {
	    agentId = request.getParameter("id");
	    return agentId;
	} else {
	    agentId = "";
	    return agentId;
	}
    }
    public static void main(String[] args) throws Exception {
	Server server = new Server(8080);
	server.setHandler(new JettyMethods());
	server.start();
	server.join();
    }
}
