package org.telelogx.teletracker.license;

import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.security.auth.x500.X500Principal;

import cblt.LicenseContent;

import com.colureware.commons.license.LicenseGenerator;

/**
 * @author Ahmad
 *
 */
public class TeleTrackerLicenseGenerater extends LicenseGenerator {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String path = "C:\\Users\\Ahmad\\Desktop\\licenses\\props.properties";
		(new TeleTrackerLicenseGenerater()).generateLicense(path);
	}

	@Override
	public LicenseContent createLicenseContent(Properties properties) {
		
		String subject = properties.getProperty("subject");
		String usingFirm = properties.getProperty("usingFirm");
		String serverid = properties.getProperty("serverId");
		Integer numOfAgents = Integer.parseInt(properties
				.getProperty("numberOfAgents"));
		int numberOfDays = Integer
				.parseInt(properties.getProperty("numOfDays"));
		TeleTrackerLicenses result = new TeleTrackerLicenses();

		X500Principal holder = new X500Principal("CN=" + usingFirm);
		result.setHolder(holder);
		X500Principal issuer = new X500Principal(
				"CN=Ahmad, OU=TeleVox, O=Telelogx, L=Amman, ST=AMM, C=JO");
		result.setIssuer(issuer);
		result.setConsumerAmount(1);
		result.setConsumerType("User");
		result.setInfo("");
		Date now = new Date();
		result.setIssued(now);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, numberOfDays);
		result.setNotAfter(calendar.getTime());
		result.setSubject(subject);
		result.setRunableAny(false);
		result.setUID(serverid);
		result.setAgentNumber(numOfAgents);
		return result;
		
	}
}
