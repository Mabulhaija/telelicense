package org.telelogx.teletracker.license;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

public class TeletrackerLicenseDAO  {
    private static final int AUTHORIZED_AGENTS_COUNT = 10;
    private Connection connection;
    private ResultSet resultSet;
    private String url = "jdbc:mysql://localhost:3306/teletracker_agents_id";
    private String userName = "root";
    private String passWord = "123456";
    AgentIdDesEncryptor Encryptor = new AgentIdDesEncryptor();
    public TeletrackerLicenseDAO() {
	try {

	    connection = DriverManager.getConnection(url, userName, passWord);
	} catch (SQLException e) {
	    System.out.println("Cant establish a connection to DB.");
	    e.printStackTrace();
	}
    }

    public void listAgentsInDB(HttpServletResponse response) {
	String preparedList = "SELECT * FROM agents;";
	PreparedStatement pstmt;
	try {
	    pstmt = connection.prepareStatement(preparedList);

	    resultSet = pstmt.executeQuery();
	    response.getWriter().println("<h1>Authorized Agents:</h1>");
	    while (resultSet.next()) {

		response.getWriter().println("<h2>" + resultSet.getString(1) + "</h2>");
	    }
	} catch (SQLException | IOException e) {

	    e.printStackTrace();
	}
    }

    public void addAgentToDB(HttpServletResponse response, String agnt)  {
	try{
	Statement stmt = connection.createStatement();
	    stmt.executeUpdate("Insert INTO agents (AgentID)VALUES('" + agnt + "');");
	    response.getWriter().println("<h2>New record Added successfully</h2>");
	    traceOperation("ADD", agnt, "Success", "");
	    response.setStatus(HttpServletResponse.SC_OK);
	}
    catch (SQLException|IOException e) {
	    try {
	    response.getWriter().println("<h1>This AgentID Exist .</h1>");
	    } catch (IOException e1) {
		traceOperation("ADD", agnt, "Failed", "Error While write a response");
		e1.printStackTrace();
	    }
	traceOperation("ADD", agnt, "Failed", "This AgentID Exist");
	response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
	    e.printStackTrace();
	}
    }

    public void traceOperation(String operation, String agent, String status, String desc) {
	try {
	Date currentDate = new Date();
	SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	InetAddress ip = InetAddress.getLocalHost();
	String ipAddress = ip.toString();
	String operationDate = dateFormatter.format(currentDate);
	String operationTime = timeFormatter.format(currentDate);
	String insertOperationQuery = "INSERT INTO trace_operations(operation,AgentID,IPaddress,Date,Time,Status,Description)"
		+ " VALUES ('"
		+ operation
		+ "','"
		+ agent
		+ "','"
		+ ipAddress
		+ "','"
		+ operationDate
		+ "','"
		+ operationTime + "','" + status + "','" + desc + "');";
	PreparedStatement pstmt = connection.prepareStatement(insertOperationQuery);
	    pstmt.executeUpdate();
	} catch (SQLException | IOException e) {
	    e.printStackTrace();

	}

    }

    public void traceAuthrization(String agent, String status, String desc) {
	try {
	Date currentDate = new Date();
	SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	InetAddress ip = InetAddress.getLocalHost();
	String operation = "AUTHORIZATION";
	String ipAddress = ip.toString();
	String operationDate = dateFormatter.format(currentDate);
	String operationTime = timeFormatter.format(currentDate);
	String insertOperationQuery = "INSERT INTO trace_authorization(operation,AgentID,IPaddress,Date,Time,Status,Description)"
		+ " VALUES ('"
		+ operation
		+ "','"
		+ agent
		+ "','"
		+ ipAddress
		+ "','"
		+ operationDate
		+ "','"
		+ operationTime + "','" + status + "','" + desc + "');";
	PreparedStatement pstmt = connection.prepareStatement(insertOperationQuery);
	pstmt.executeUpdate();
	} catch (SQLException | UnknownHostException e) {
	    e.printStackTrace();
	}
    }

    public void checkAgentIDBeforAdd(HttpServletResponse response, String agent) {
	try {
	String queryToGetAgentCount = "SELECT count(*) FROM agents";
	PreparedStatement countAgents = connection.prepareStatement(queryToGetAgentCount);
	resultSet = countAgents.executeQuery();
	resultSet.next();
	int counter = resultSet.getInt(1);
	    if (counter >= AUTHORIZED_AGENTS_COUNT) {

	    response.getWriter().println(
		    "<h1>your license disallow you from adding more Agents.You have to call your provider.</h1>");
	    traceOperation("ADD", agent, "Failed", "Max number of Authorized agents reached.");
	    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
	} else {

	    addAgentToDB(response, agent);
	}
	} catch (SQLException | IOException e) {
	    traceOperation("ADD", agent, "Failed", "Error While connecting to DB.");
	    e.printStackTrace();
	}
    }

    public void removeAgentFromDB(HttpServletResponse response, String agentId) {
	try {
	    String queryToRemoveAgent = "DELETE FROM agents WHERE AgentID = ?";
	PreparedStatement removeAgent = connection.prepareStatement(queryToRemoveAgent);
	removeAgent.setString(1, agentId);
	int affectedRows = removeAgent.executeUpdate();
	if (affectedRows == 0) {
	    response.getWriter().println("<h1>This AgentID does not exist.</h1>");
	    traceOperation("REMOVE", agentId, "Failed", "AgentId does not exist.");
	    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
	} else {
	    response.getWriter().println("<h1>record Removed successfully.</h1>");
	    traceOperation("REMOVE", agentId, "Success", "");
	    response.setStatus(HttpServletResponse.SC_OK);
	    }
	} catch (SQLException | IOException e) {
	    traceOperation("REMOVE", agentId, "Failed", "Error While connecting to DB.");
	    e.printStackTrace();
	}
    }

    public void authorizationChecker(String originalAgent, String saltedAgent, String mixedDummyString,
	    HttpServletResponse response) {
	try {
	String checkAgent = "SELECT * FROM agents WHERE AgentID = ? "; 
	 PreparedStatement authcheck = connection.prepareStatement(checkAgent);
	authcheck.setString(1, originalAgent);
	 resultSet = authcheck.executeQuery();

	if (resultSet.next()) {
	    response.getWriter().println(Encryptor.Encrypt(saltedAgent));
	    traceAuthrization(originalAgent, "Success", "");
			}
	else{
	    response.getWriter().println(Encryptor.Encrypt(mixedDummyString));
	    traceAuthrization(originalAgent, "Failed", "this Agent Not Authrized");
	   }
	} catch (SQLException | IOException e) {
	    traceAuthrization(originalAgent, "Failed", "Error While connecting to DB.");
	    e.printStackTrace();
	   }
    }
}
