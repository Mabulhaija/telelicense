package org.telelogx.teletracker.license;

import com.colureware.license.impl.ColurewareLicense;

/**
 * @author Ahmad
 *
 */
public class TeleTrackerLicenses extends ColurewareLicense {
	/**
	 * 
	 */
	int agentNumber;

	/**
	 * @return
	 */
	public int getAgentNum() {
		return agentNumber;
	}

	/**
	 * @param agentNumber
	 */
	public void setAgentNumber(int agentNumber) {
		this.agentNumber = agentNumber;
	}
}
