package org.telelogx.teletracker.license;

import java.io.File;

import com.colureware.license.LicenseInstallationManager;
import com.colureware.license.LicenseVerificationManager;
import com.colureware.license.impl.LicenseInstallationManagerImpl;
import com.colureware.license.impl.LicenseVerificationManagerImpl;

/**
 * @author Ahmad
 *
 */
public class TestTeletrackerLienc {

	static LicenseInstallationManager licenseInstallationManager = new LicenseInstallationManagerImpl();
	static LicenseVerificationManager licenseVerificationManager = new LicenseVerificationManagerImpl();

	public static void main(String[] args) {

		licenseInstallationManager.installLicense(new File("C:\\Users\\Ahmad\\Desktop\\licenses\\teleTracker.lic.base64"));
		TeleTrackerLicenses teleTrackerLicenses = (TeleTrackerLicenses) licenseVerificationManager
				.verify();
		System.out.println(teleTrackerLicenses.getAgentNum());

	}

}
